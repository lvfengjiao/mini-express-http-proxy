function filter(container){
    if(!container.user.userOptions || !container.user.userOptions.filter) return Promise.resolve(container);
    const filterFn = container.user.userOptions.filter;
    const result = filterFn(container.options.req, container.options.res);

    return result? Promise.resolve(container): Promise.reject(container);
}
module.exports = filter;