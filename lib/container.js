function Container(req, res, target, userOptions){
    // handle syntax error
    if(typeof target!=="string") {
        throw new Error('target is required');
    }
    if(userOptions && userOptions.intercept) {
        throw new Error("intercept has been outdated, please use decroResAsync instead");
    }
    return {
        options: {
            req,
            res
        },
        reqBuilder: {
            req,
            res
        },
        user: {
            target,
            userOptions
        }
    };
}
module.exports = Container;