function request(container) {
  const options = container.reqBuilder.req;
  return new Promise((resolve, reject) => {
    const req = http.request(options, (res) => {
      const chunks = [];
      console.log(`STATUS: ${res.statusCode}`);
      console.log(`HEADERS: ${JSON.stringify(res.headers)}`);
      res.setEncoding("utf8");
      res.on("data", (chunk) => {
        chunks.push(chunk);
      });
      res.on("end", () => {
        console.log("No more data in response.");
        container.proxy.res = chunks.toString();
        resolve(container);
      });
    });

    req.on("error", (e) => {
      console.log(`problem with request: ${e.message}`);
    });

    // write data to request body
    req.write(postData);
    req.end();
  });
}
module.exports = request;
