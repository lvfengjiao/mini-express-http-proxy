function decroReqAsync(container) {
  if (!container.user.userOptions || !container.user.userOptions.decroReqAsync)
    return Promise.resolve(container);
  const { decroReqAsync } = container.user.userOptions;
  return Promise.resolve(decroReqAsync(...container.options)).then((processReq)=>{
      container.reqBuilder.req = {...container.reqBuilder.req, processReq};
    return container;
  })
  
}
module.exports = decroReqAsync;
