function decroResAsync(container) {
    if (!container.user.userOptions || !container.user.userOptions.decroResAsync)
      return Promise.resolve(container);
    const { decroResAsync } = container.user.userOptions;
    return Promise.resolve(decroResAsync(...container.options)).then((process)=>{
        container.proxy.bodyContent = process;
      return container;
    })
    
  }
  module.exports = decroResAsync;
  