const Container = require('./lib/container');
const request = require('./lib/request');
const filter = require('./lib/filter');
const decroReqAsync = require('./lib/decroReqAsync');
const decroResAsync = require('./lib/decroResAsync');

const proxy = (target, userOptions) => {
    return function(req, res, next){
        const container = new Container(req, res, target, userOptions);
        filter(container)
        .then(decroReqAsync)
        .then(request)
        .then(decroResAsync)
        .catch(err=>{
            if(err) {
                res.body = err;
            }

            next();
        })
    }
}
module.export = proxy;